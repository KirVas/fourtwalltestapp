package by.kirvas.fourthwalltest.data.remote

import androidx.paging.PagingSource
import androidx.paging.PagingState
import by.kirvas.fourthwalltest.data.model.Picture
import by.kirvas.fourthwalltest.data.remote.service.PicsumService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class PicsumPagingSource(
    private val picsumService: PicsumService
    ) : PagingSource<Int, Picture>() {

    override fun getRefreshKey(state: PagingState<Int, Picture>): Int? {
        val anchorPosition = state.anchorPosition ?: return null
        val anchorPage = state.closestPageToPosition(anchorPosition) ?: return null
        return anchorPage.prevKey?.plus(1) ?: anchorPage.nextKey?.minus(1)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Picture> {
        try {
            val pageNumber = params.key ?: 1
            val pageSize = params.loadSize

            var pictures : List<Picture>? = null
            var error : Throwable? = null

            withContext(Dispatchers.IO){
                try {
                    picsumService.getPictures(pageNumber, pageSize).execute().body()?.let {
                        pictures = it
                    }
                } catch (e: Exception) {
                    error = e
                }
            }

            error?.let {
                return LoadResult.Error(it)
            }

            val nextPageNumber = if (pictures!!.isEmpty()) null else pageNumber + 1
            val prevPageNumber = if (pageNumber > 1) pageNumber - 1 else null
            return LoadResult.Page(pictures!!, prevPageNumber, nextPageNumber)

        } catch (e: HttpException) {
            return LoadResult.Error(e)
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }

}