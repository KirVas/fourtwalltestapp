package by.kirvas.fourthwalltest.data.repository

import androidx.paging.PagingSource
import by.kirvas.fourthwalltest.data.model.Picture
import by.kirvas.fourthwalltest.data.remote.Api
import by.kirvas.fourthwalltest.data.remote.PicsumPagingSource

object PicturesRepository {
    fun getPictures(): PagingSource<Int, Picture> = PicsumPagingSource(Api.picsumApi)
}