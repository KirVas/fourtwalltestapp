package by.kirvas.fourthwalltest.data.remote.service

import by.kirvas.fourthwalltest.data.model.Picture
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PicsumService {

    @GET("v2/list")
    fun getPictures(
        @Query("page") page: Int,
        @Query("limit") itemsPerPage: Int
    ) : Call<List<Picture>>
}