package by.kirvas.fourthwalltest.data.remote

import by.kirvas.fourthwalltest.BuildConfig
import by.kirvas.fourthwalltest.data.remote.service.PicsumService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Api {

    private val retrofit : Retrofit by lazy {
        val httpClient = OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .writeTimeout(5, TimeUnit.SECONDS)
            .build()

        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }



    val picsumApi = retrofit.create(PicsumService::class.java)
}