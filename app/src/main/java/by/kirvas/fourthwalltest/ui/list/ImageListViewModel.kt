package by.kirvas.fourthwalltest.ui.list

import android.app.Application
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import by.kirvas.fourthwalltest.domain.GetPicturesUseCase
import by.kirvas.fourthwalltest.ui.BaseViewModel

class ImageListViewModel(application: Application) : BaseViewModel(application) {

    val picturesFlow = Pager(
        PagingConfig(
            initialLoadSize = 20,
            pageSize = 20,
            enablePlaceholders = false
        )
    ) {
        GetPicturesUseCase.invoke()
    }.flow.cachedIn(viewModelScope)

}