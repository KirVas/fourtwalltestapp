package by.kirvas.fourthwalltest.ui

import android.app.Application
import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

abstract class BaseViewModel(private val mApplication: Application) : AndroidViewModel(mApplication) {

    private val _isLoading = MutableLiveData<Boolean>()
    open val isLoading: LiveData<Boolean> = _isLoading

    protected val resources: Resources get() = mApplication.resources

    protected fun getString(@StringRes stringRes: Int, vararg args: Any?) =
        resources.getString(stringRes, args)

    fun setLoading(loading: Boolean) {
        _isLoading.value = loading
    }

}