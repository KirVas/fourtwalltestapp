package by.kirvas.fourthwalltest.ui.list

import android.os.Build
import android.view.LayoutInflater
import androidx.core.view.isVisible
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import by.kirvas.fourthwalltest.BR
import by.kirvas.fourthwalltest.NavMainDirections
import by.kirvas.fourthwalltest.data.model.Picture
import by.kirvas.fourthwalltest.databinding.FragmentImageListBinding
import by.kirvas.fourthwalltest.ui.BaseFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import android.util.DisplayMetrics
import kotlin.math.roundToInt


class ImageListFragment : BaseFragment<FragmentImageListBinding, ImageListViewModel>(ImageListViewModel::class) {

    private val recyclerAdapter by lazy {
        PicturesRecyclerAdapter(requireContext(), ::onItemSelected)
    }

    override fun setupViews() {
        binding.setVariable(BR.viewModel, viewModel)
        with(binding.rvPictures) {
            adapter = recyclerAdapter.withLoadStateFooter(PicturesLoadStateAdapter(requireContext()))
            layoutManager = GridLayoutManager(context, calculateColumnsCount())
        }
        recyclerAdapter.addLoadStateListener {
            binding.progressBar.isVisible = it.refresh is LoadState.Loading
            binding.swipeRefresh.isRefreshing = it.refresh is LoadState.Loading
            binding.tvError.isVisible = it.refresh is LoadState.Error
        }
        binding.swipeRefresh.setOnRefreshListener {
            recyclerAdapter.refresh()
        }

    }

    override fun observeViewModel(viewModel: ImageListViewModel) {
        lifecycleScope.launch {
            viewModel.picturesFlow.collectLatest(recyclerAdapter::submitData)
        }
    }

    override val bindingInflater: (LayoutInflater) -> ViewDataBinding
        get() = FragmentImageListBinding::inflate

    private fun onItemSelected(picture: Picture?) {
        picture?.let {
            navigate(NavMainDirections.actionGlobalImageDetailsFragment(it.downloadUrl, it.author))
        }
    }

    private fun calculateColumnsCount() : Int {
        val width = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val metrics = activity?.windowManager?.currentWindowMetrics
            metrics?.bounds?.width()?:0
        } else {
            val metrics = DisplayMetrics()
            activity?.windowManager?.defaultDisplay?.getMetrics(metrics)
            metrics.widthPixels
        }
        val x = (width / ONE_ITEM_REFERENTIAL_WIDTH).roundToInt()
        return if (x > 2) x else 2
    }

    companion object {
        const val ONE_ITEM_REFERENTIAL_WIDTH = 450.0
    }
}