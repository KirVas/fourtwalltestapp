package by.kirvas.fourthwalltest.ui.details

import android.view.LayoutInflater
import androidx.databinding.ViewDataBinding
import by.kirvas.fourthwalltest.databinding.FragmentImageDetailsBinding
import by.kirvas.fourthwalltest.ui.BaseFragment
import com.bumptech.glide.Glide
import by.kirvas.fourthwalltest.BR
import by.kirvas.fourthwalltest.R
import android.graphics.Bitmap

import android.widget.Toast
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import android.content.Intent
import android.net.Uri

import androidx.core.content.FileProvider
import androidx.core.view.drawToBitmap

class ImageDetailsFragment : BaseFragment<FragmentImageDetailsBinding, ImageDetailsViewModel>(ImageDetailsViewModel::class) {

    override val bindingInflater: (LayoutInflater) -> ViewDataBinding
            = FragmentImageDetailsBinding::inflate

    override fun setupViews() {
        binding.setVariable(BR.viewModel, viewModel)
        viewModel.setupData(
            arguments?.getString("url"),
            arguments?.getString("author")
        )

        Glide.with(this)
            .load(viewModel.url.value)
            .into(binding.ivPicture)
    }

    override fun observeViewModel(viewModel: ImageDetailsViewModel) {
        viewModel.shareImageClicked.observe(viewLifecycleOwner){
            if (it) {
                createCachedBitmap()
                shareCachedBitmap()
            }
        }
    }

    private fun createCachedBitmap() {
        try {
            val bitmap = binding.ivPicture.drawToBitmap()
            val cachePath = File(requireContext().cacheDir, "images")
            cachePath.mkdirs()
            val stream = FileOutputStream("$cachePath/image.png")
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            stream.close()
        } catch (e: IOException) {
            Toast.makeText(requireContext(), R.string.generic_error, Toast.LENGTH_SHORT).show()
        }
    }

    private fun shareCachedBitmap() {
        val imagePath = File(requireContext().cacheDir, "images")
        val newFile = File(imagePath, "image.png")
        val contentUri: Uri = FileProvider.getUriForFile(requireContext(), "by.kirvas.fourthwalltest.fileprovider", newFile)

        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        shareIntent.setDataAndType(contentUri, requireContext().contentResolver.getType(contentUri))
        shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri)
        startActivity(Intent.createChooser(shareIntent, getString(R.string.title_share_image)))
    }
}