package by.kirvas.fourthwalltest.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import kotlin.reflect.KClass

abstract class BaseFragment<VB : ViewDataBinding, ModelT : BaseViewModel>
    (viewModelClass: KClass<ModelT>) : Fragment() {

    private var _binding: ViewDataBinding? = null
    abstract val bindingInflater: (LayoutInflater) -> ViewDataBinding

    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    protected val navController get() = Navigation.findNavController(requireView())

    protected open val viewModel by lazy {
        ViewModelProvider(this)[viewModelClass.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = bindingInflater.invoke(inflater)
        return requireNotNull(_binding).root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        observeViewModel(viewModel)
    }

    protected open fun setupViews() {

    }

    protected open fun observeViewModel(viewModel: ModelT) {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    protected fun navigate(navDirections: NavDirections) = navController.navigate(navDirections)
    protected fun navigateUp() = navController.navigateUp()
    protected fun popBackStack() = navController.popBackStack()
}
