package by.kirvas.fourthwalltest.ui.details

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import by.kirvas.fourthwalltest.ui.BaseViewModel

class ImageDetailsViewModel(application: Application) : BaseViewModel(application) {
    private val _author = MutableLiveData("")
    val author: LiveData<String> = _author

    private val _url = MutableLiveData("")
    val url: LiveData<String> = _url

    private val _shareImageClicked = MutableLiveData(false)
    val shareImageClicked: LiveData<Boolean> = _shareImageClicked

    fun setupData(url: String?, author: String?) {
        url?.let { _url.value = it }
        author?.let { _author.value = it }
    }

    fun share() {
        _shareImageClicked.value = true
        _shareImageClicked.value = false
    }
}