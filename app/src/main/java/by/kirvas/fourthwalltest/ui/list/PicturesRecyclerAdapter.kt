package by.kirvas.fourthwalltest.ui.list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import by.kirvas.fourthwalltest.R
import by.kirvas.fourthwalltest.data.model.Picture
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class PicturesRecyclerAdapter (context: Context, private val onClick: (Picture?) -> Unit) :
    PagingDataAdapter<Picture, PictureViewHolder>(PictureDiffItemCallback) {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureViewHolder {
        return PictureViewHolder(layoutInflater.inflate(R.layout.item_rv_pictures, parent, false))
    }

    override fun onBindViewHolder(holder: PictureViewHolder, position: Int) {
        holder.bind(getItem(position), onClick)
    }
}

class PictureViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(picture: Picture?, onClick: (Picture?) -> Unit) {
        val ivPicture = itemView.findViewById<ImageView>(R.id.iv_picture)
        if (picture != null) {
            Glide.with(ivPicture.context)
                .load(picture.downloadUrl)
                .placeholder(R.drawable.ic_loading)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .into(ivPicture)
        } else {
            Glide.with(ivPicture.context).clear(ivPicture)
        }
        itemView.setOnClickListener{
            onClick(picture)
        }
    }
}

private object PictureDiffItemCallback : DiffUtil.ItemCallback<Picture>() {

    override fun areItemsTheSame(oldItem: Picture, newItem: Picture): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Picture, newItem: Picture): Boolean {
        return oldItem.id == newItem.id && oldItem.url == newItem.url
    }
}
