package by.kirvas.fourthwalltest.ui.list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import by.kirvas.fourthwalltest.R

class PicturesLoadStateAdapter(context: Context) : LoadStateAdapter<PicturesLoadStateAdapter.ItemViewHolder>(){

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): ItemViewHolder {
        return ItemViewHolder(layoutInflater.inflate(R.layout.item_state_rv_pictures, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(loadState: LoadState) {
            when (loadState) {
                is LoadState.Error -> {
                    itemView.findViewById<ProgressBar>(R.id.progressBar).isVisible = false
                    itemView.findViewById<TextView>(R.id.tv_error).isVisible = true
                }
                is LoadState.Loading -> {
                    itemView.findViewById<ProgressBar>(R.id.progressBar).isVisible = true
                    itemView.findViewById<TextView>(R.id.tv_error).isVisible = false
                }
                else -> {
                    itemView.findViewById<ProgressBar>(R.id.progressBar).isVisible = false
                    itemView.findViewById<TextView>(R.id.tv_error).isVisible = false
                }
            }
        }
    }
}