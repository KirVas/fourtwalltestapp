package by.kirvas.fourthwalltest.domain

import androidx.annotation.WorkerThread
import by.kirvas.fourthwalltest.data.repository.PicturesRepository

object GetPicturesUseCase {
    @WorkerThread
    operator fun invoke() = PicturesRepository.getPictures()
}