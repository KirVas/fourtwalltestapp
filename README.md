# README #

This is a coding challenge application for FourthWall made by Kiryl Vasilevich
Here is a [task description](https://gist.github.com/wbaumann/5a3524d5260a0ed056cc52b783726d3c)

### What is done? ###
* When the app launches, a loading 🌀 state should appear during the initial loading stage (e.g. API requests are made)
* Once you've retrieved a valid list of images, we should display each in a 2-column grid of square images
* Whenever the user taps on an image, a new screen (image detail) should appear with the following: A larger view of the image and the name of the image's author.
* Additionally, the image detail screen should also include a button, which will allow the user to share the image with other apps (e.g. email, SMS, etc)


### Additional functionality ###
* Handling error states if the API fails to load
* Caching layer to speed up cold launches (Glide caching)
* Support for paging, so we automatically load more images as the user scrolls up/down (Paging 3)
* Using different amounts of grid columns, based on if the phone is in portrait vs landscape mode
* Using different amounts of grid columns, based on if we're using an phone or tablet